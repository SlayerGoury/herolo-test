from django.contrib import auth
from django.db import IntegrityError
from django.http import HttpResponse
from django.shortcuts import render
from django.views.decorators.csrf import csrf_exempt
from django.views.decorators.http import require_http_methods
from .models import *
import json
import re

DATETIME_FORMAT = '%Y-%m-%d T%H:%M'
uuid4hex = re.compile('[0-9a-f]{12}4[0-9a-f]{3}[89ab][0-9a-f]{15}\Z', re.I)


def index(request):
    return render(request, 'index.html', {})

@require_http_methods('POST')
@csrf_exempt
def login(request):
    """ Authernticate using username and password """
    username = request.POST.get('username')
    password = request.POST.get('password')
    user = auth.authenticate(username=username, password=password)
    if user is not None and user.is_active:
        auth.login(request, user)
        response, status = {'request': 'login', 'result': 'success'}, 200
    else:
        response, status = {'request': 'login', 'result': 'fail'}, 403
    return HttpResponse( json.dumps(response), status=status )


@require_http_methods('POST')
@csrf_exempt
def write(request):
    """
        Message writer, accepts following POST parameters:
        * sent_to: to whom the message have to be sent, will be sent to nobody if user does not exist
        * message: message text
        * subject: optional subject
        Message will have sent_by parameter set up to currently authenticated user or None if session is not authenticated (see login)
    """
    sent_by = None if not request.user.is_authenticated else request.user
    sent_to = request.POST.get('sent_to')
    message = request.POST.get('message')
    subject = request.POST.get('subject')
    receiver = auth.models.User.objects.filter(username=sent_to).first()
    try:
        result = Message.objects.create(
            sent_by = sent_by,
            sent_to = receiver,
            message = message,
            subject = subject,
        )
        response, status = {'request': 'write', 'result': 'success', 'uuid': str(result.uuid)}, 200
    except IntegrityError as e:
        response, status = {'request': 'write', 'result': 'fail', 'reason': e.message}, 400

    return HttpResponse( json.dumps(response), status=status )


def _message_dict(message):
    username_by = message.sent_by.username if message.sent_by else 'nobody'
    username_to = message.sent_to.username if message.sent_to else 'nobody'
    return {
        'sent_by': username_by,
        'sent_to': username_to,
        'message': message.message,
        'subject': message.subject,
        'date_created': message.date_created.strftime(DATETIME_FORMAT),
        'read': message.read,
    }


@csrf_exempt
def get_messages(request):
    """
        Messages list, accepts following GET parameters:
        * unread: output only unread messages, default is False
        To read messages sent for a specific existing user session have to be authenticated (see login)
    """
    # TODO pagination
    sent_to = None if not request.user.is_authenticated else request.user
    messages = Message.objects.filter(sent_to=sent_to)
    if request.GET.get('unread'):
        messages = messages.filter(read=False)
    if not messages:
        response, status = {'request': 'get_messages', 'result': 'fail', 'reason': 'no messages to return'}, 204
    else:
        response, status = {'request': 'get_messages', 'result': 'success'}, 200
    blob = {}
    for message in messages:
        blob[str(message.uuid)] = _message_dict(message)
    response['messages'] = blob
    return HttpResponse( json.dumps(response), status=status )


def _get_message(request, delete=False):
    target = 'delete_message' if delete else 'read_message'
    uuid = request.POST.get('uuid') or request.GET.get('uuid')
    if not uuid or not uuid4hex.match(uuid.replace('-', '')):
        return {'request': target, 'result': 'fail', 'reason': 'bad uuid'}, 400

    try:
        message = Message.objects.get(uuid=uuid)
    except Message.DoesNotExist:
        # TODO mark messages as deleted instead of deleteng and return 410 status
        return {'request': target, 'result': 'fail', 'reason': 'message does not exist'}, 404

    if message.sent_by and message.sent_to:
        if message.sent_by != request.user and message.sent_to != request.user:
            return {'request': target, 'result': 'fail', 'reason': 'access denied'}, 401

    if delete:
        if message.sent_to != request.user:
            if message.read:
                return {'request': target, 'result': 'fail', 'reason': 'can not delete read message that is not yours'}, 401
        message.delete()
        return {'request': target, 'result': 'success', 'message': 'message deleted'}, 200

    if message.sent_to == request.user:
        message.read = True
        message.save()

    return {'request': target, 'result': 'success', 'message': _message_dict(message)}, 200


@csrf_exempt
def read(request):
    """
        Read message, accepts following POST or GET parameters:
        * uuid: message id to be requested
        To read a message you should be authenticated either as sender or as reveiver, unless message is sent to or by nobody
    """
    response, status = _get_message(request)  # because of DRY
    return HttpResponse( json.dumps(response), status=status )



@require_http_methods('POST')
@csrf_exempt
def delete(request):
    """
        Delete message, accepts following POST or GET parameters:
        * uuid: message id to be deleted
        To delete a message you should be authenticated either as sender or as reveiver, unless message is sent to or by nobody
        Sender can only delete unread messages
    """
    response, status = _get_message(request, delete=True)  # because of DRY
    return HttpResponse( json.dumps(response), status=status )
