from django.contrib.auth.models import AnonymousUser, User
from django.contrib.sessions.backends.db import SessionStore
from django.test import TestCase, RequestFactory
from . import models, views
import json

# TODO comment test cases


class HeroloTests(TestCase):
    def setUp(self):
        self.factory = RequestFactory()
        self.user1 = User.objects.create_user(username='meow1', email='1m@e.ow', password='purr')
        self.user2 = User.objects.create_user(username='meow2', email='2m@e.ow', password='purr')
        self.user3 = User.objects.create_user(username='meow3', email='3m@e.ow', password='purr')

    def test_login(self):
        request = self.factory.post('/login', {'username': 'meow1', 'password': 'purr'})
        request.session = SessionStore()
        response = views.login(request)
        self.assertEqual( json.loads(response.content), {'request': 'login', 'result': 'success'} )
        request = self.factory.post('/login', {'username': 'meow1', 'password': 'wrong password'})
        request.session = SessionStore()
        response = views.login(request)
        self.assertEqual( json.loads(response.content), {'request': 'login', 'result': 'fail'} )

    def test_write(self):
        request = self.factory.post('/write', {'sent_to': 'meow2', 'subject': 'purr', 'message': 'murr'})
        request.session = SessionStore()
        request.user = self.user1
        response = views.write(request)
        message = models.Message.objects.get(uuid=json.loads(response.content)['uuid'])
        self.assertEqual(message.sent_by, self.user1)
        self.assertEqual(message.sent_to, self.user2)
        self.assertEqual(message.read, False)
        self.assertEqual(message.message, 'murr')
        self.assertEqual(message.subject, 'purr')

    def test_get_messages(self):
        models.Message.objects.create(sent_to=self.user1, sent_by=None, message='purr', subject='meow')
        models.Message.objects.create(sent_to=self.user2, sent_by=None, message='purr', subject='meow')
        models.Message.objects.create(sent_to=self.user3, sent_by=None, message='purr', subject='meow')
        models.Message.objects.create(sent_to=self.user1, sent_by=self.user2, message='purr', subject='meow')
        models.Message.objects.create(sent_to=self.user2, sent_by=self.user2, message='purr', subject='meow')
        models.Message.objects.create(sent_to=self.user3, sent_by=self.user2, message='purr', subject='meow')
        models.Message.objects.create(sent_to=self.user1, sent_by=self.user3, message='purr', subject='meow', read=True)
        models.Message.objects.create(sent_to=self.user2, sent_by=self.user3, message='purr', subject='meow', read=True)
        models.Message.objects.create(sent_to=self.user3, sent_by=self.user3, message='purr', subject='meow', read=True)
        request = self.factory.post('/get_messages', {})
        request.user = AnonymousUser()
        request.session = SessionStore()
        response = views.get_messages(request)
        self.assertEqual( response.status_code, 204 )
        request.user = self.user1
        response = views.get_messages(request)
        self.assertEqual( len(json.loads(response.content)['messages']), 3)
        request.user = self.user2
        response = views.get_messages(request)
        self.assertEqual( len(json.loads(response.content)['messages']), 3)
        request.user = self.user3
        response = views.get_messages(request)
        self.assertEqual( len(json.loads(response.content)['messages']), 3)
        request = self.factory.post('/get_messages?unread=1', {})
        request.session = SessionStore()
        request.user = self.user1
        response = views.get_messages(request)
        self.assertEqual( len(json.loads(response.content)['messages']), 2)
        request.user = self.user2
        response = views.get_messages(request)
        self.assertEqual( len(json.loads(response.content)['messages']), 2)
        request.user = self.user3
        response = views.get_messages(request)
        self.assertEqual( len(json.loads(response.content)['messages']), 2)

    def test_read(self):
        message = models.Message.objects.create(sent_to=self.user1, sent_by=self.user2, message='purr', subject='meow')
        request = self.factory.post('/read', {'uuid': str(message.uuid)})
        request.session = SessionStore()
        request.user = AnonymousUser()
        response = views.read(request)
        self.assertEqual(response.status_code, 401)
        request.user = self.user2
        response = views.read(request)
        self.assertEqual(response.status_code, 200)
        message.refresh_from_db()
        self.assertFalse(message.read)
        request.user = self.user1
        response = views.read(request)
        self.assertEqual(response.status_code, 200)
        message.refresh_from_db()
        self.assertTrue(message.read)

    def test_delete(self):
        message = models.Message.objects.create(sent_to=self.user1, sent_by=self.user2, message='purr', subject='meow', read=True)
        request = self.factory.post('/delete', {'uuid': str(message.uuid)})
        request.session = SessionStore()
        request.user = self.user2
        response = views.delete(request)
        self.assertEqual(response.status_code, 401)
        request.user = self.user1
        response = views.delete(request)
        self.assertEqual(response.status_code, 200)
