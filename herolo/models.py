from django.contrib.auth.models import User
from django.db import models
import datetime
import uuid


class Message(models.Model):
	uuid          = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
	sent_by       = models.ForeignKey(User, related_name='sent_by', blank=True, null=True, on_delete=models.SET_NULL)
	sent_to       = models.ForeignKey(User, related_name='sent_to', blank=True, null=True, on_delete=models.SET_NULL)
	message       = models.TextField()
	subject       = models.CharField(max_length=255)
	date_created  = models.DateTimeField(auto_now_add=True)
	read          = models.BooleanField(default=False)
