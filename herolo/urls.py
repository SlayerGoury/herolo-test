from django.contrib import admin
from django.urls import path
from . import views

urlpatterns = [
    path('admin/',        admin.site.urls),
    path('login',         views.login,         name="api_login"),
    path('write',         views.write,         name="api_write"),
    path('get_messages',  views.get_messages,  name="api_get_messages"),
    path('read',          views.read,          name="api_read"),
    path('delete',        views.delete,        name="api_delete"),
]
